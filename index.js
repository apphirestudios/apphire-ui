"use strict";

if (typeof window !== "undefined" && window !== null) {
  module.exports = require('./lib');
} else {
  throw new Error('This module is clientside only');
}
