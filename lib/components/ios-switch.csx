utils = require '../utils'

module.exports =
  controller: ->
    vm = @
    vm.key = utils.guid()
    return vm
  view: (vm, attrs, children) ->
    span
      key: vm.key
      input(type='checkbox')
        onchange: (e)->
          attrs.model e.target.checked
        config: (el, inited, ctx) ->
          if not inited
            el.checked = attrs.model()
            ctx.switchery = new Switchery(el)

          if attrs.model() isnt ctx.switchery.isChecked()
            ctx.switchery.setPosition(true)
            ctx.switchery.handleOnchange(true)




