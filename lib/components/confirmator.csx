utils = require '../utils'
view = require 'apphire-view'

module.exports = confirmator =
  message: undefined
  header: undefined
  ask: (message) ->
    log 'ASK ' + message.body
    confirmator.message = message.body
    view.redraw()
    confirmator.promise = new Promise (resolve, reject)->
      view.compute ->
        if message.body
          confirmator.header = message.header
          confirmator.message = message.body
        else
          confirmator.message = message
      confirmator.resolve = resolve
      confirmator.reject = reject
    confirmator.promise.then ->
      view.compute ->
        confirmator.message = undefined
        confirmator.header = undefined
    return confirmator.promise

  view1: ->
    div
      if confirmator.message
        div
          h1 | 24324324
  view: (vm, attrs, children) ->
    div
      if confirmator.message
        uma-modal-box
          dialogClass: 'modal-sm'
          onClose: -> confirmator.resolve(false)
          div
            .modal-header
              'style':
                'padding-bottom': '0px'
              button.close
                type: 'button'
                onclick: -> confirmator.resolve(false)
                span | ×
                span.sr-only | Закрыть
              h4
                .modal-title
                  .row
                    i.fa.fa-exclamation-triangle.text-danger
                      'style':
                        'font-size': '50px'
                  confirmator.header? or 'Подтвердите выполнение действия'
            .modal-body
              div
                confirmator.message
            .modal-footer
              button.btn.btn-lg.btn-danger(type='button', loader=true, title='ОК')
                onclick: -> confirmator.resolve(true)
                span | ОК
                i.fa.fa-check
              button.btn.btn-lg.btn-white(type='button', loader=true, title='Отмена')
                onclick: -> confirmator.resolve(false)
                i.fa.fa-undo
                span | Отмена





