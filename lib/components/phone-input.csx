mithtrilBootstrap = require './mithril-bootstrap'
#modalBoxService = require 'services/modal-box'
require 'jquery.inputmask'

phoneTypes = []
phoneTypes.push
  name:  'mobile'
  caption: 'Мобильный'
  mask: "+7(999) 999-9999"
  icon: 'fa-mobile-phone'

phoneTypes.push
  name:  'static'
  caption: 'Стационарный'
  mask: "+7(499) 999-9999"
  icon: 'fa-phone'

phoneTypes.push
  name:  'sip'
  caption: 'IP-телефония'
  mask: '1001@sip.example.com'
  icon: 'fa-asterisk'


module.exports =
  controller: ->
    vm = @
    return @

  view: (vm, attrs, children) ->
    form.form-horizontal
      fieldset
        .form-group
          .col-sm-1.dropdown
            config: mithtrilBootstrap.ui.configDropdown()
            button.btn.btn-link.btn-lg.dropdown-toggle(type='button')
              span.fa
                className: (t for t in phoneTypes when t.name is attrs.model().phoneType())[0].icon
              span.caret
            ul.dropdown-menu
              phoneTypes.map (phoneType)->
                li
                  a
                    onclick: ->
                      attrs.model().phoneType phoneType.name
                      attrs.model().phoneNumber undefined
                      attrs.model().phoneNumber.touched false
                    phoneType.caption
          if attrs.model().phoneType() is 'static'
            .col-sm-5
              uma-text-input
                model: attrs.model().phoneNumber
                label: 'Номер телефона'
                showValidationErrors: attrs.showValidationErrors
                inputConfig: (el, inited, ctx)->
                  if not inited
                    mask = (pT for pT in phoneTypes when pT.name is attrs.model().phoneType())[0].mask
                    im = new Inputmask
                      mask: mask
                      showMaskOnHover: false
                    im.mask(el);
          else
            .col-sm-7
              uma-text-input
                model: attrs.model().phoneNumber
                label: 'Номер телефона'
                showValidationErrors: attrs.showValidationErrors
                inputConfig: (el, inited, ctx)->
                  if not inited
                    mask = (pT for pT in phoneTypes when pT.name is attrs.model().phoneType())[0].mask
                    im = new Inputmask
                      mask: mask
                      showMaskOnHover: false
                    im.mask(el);


          div
            if attrs.model().phoneType() is 'static'
              .col-sm-2
                uma-text-input
                  model: attrs.model().extensionNumber
                  label: 'Доб'

          .col-sm-3
            uma-text-input
              model: attrs.model().managerName
              label: 'Имя'
          if attrs.ondelete
            label.col-sm-1.control-label
              a.input-group-link.text-danger
                onclick: -> attrs.ondelete()
                i.fa.fa-minus-circle








