utils = require '../utils'

module.exports =
  controller: ->
    vm = @
    vm.key = utils.guid()
    return @


  view: (vm, attrs, children) ->
    .checkbox
      key: vm.key
      label
        input(type='checkbox')
          id: vm.key
          checked: attrs.model()
          onchange: (e)-> attrs.model e.currentTarget.checked
        span.checkbox-material
          span.check
        attrs.model?.schema?.caption or attrs.label
      uma-help-tooltip
        key: attrs.helpTooltipKey
