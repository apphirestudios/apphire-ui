mithrilBootstrap = require './mithril-bootstrap'
module.exports =
  controller: (attrs)->
    vm = @
    vm.totalItems = attrs.totalItems
    vm.itemsPerPage = attrs.itemsPerPage
    vm.currentPage = (page)->
      if arguments.length
        vm.currentPage.store = page
        attrs.queryFn
          skip: page*vm.itemsPerPage()
          limit: vm.itemsPerPage()
      else
        vm.currentPage.store or 0
    vm.directionLinks = true
    vm.boundaryLinks = true
    vm.previousText = '<'
    vm.nextText = '>'
    vm.maxSize = 7
    vm.firstText = if app.translator then app.translator.translate('Первый') or 'Первый'
    vm.lastText = if app.translator then app.translator.translate('Последний') or 'Последний'
    vm.pagination = mithrilBootstrap.u.init mithrilBootstrap.ui.pagination(vm)
    return @
  view: (vm, attrs, children) ->
    div
      if vm.totalItems() > vm.itemsPerPage()
        vm.pagination.$view()
