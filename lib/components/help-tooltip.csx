utils = require '../utils'
view = require 'apphire-view'

HelpTooltipResource =
  tooltips: {}

  getOrCreateTooltip: (key)-> async ->
    HelpTooltipModel = yield app.Model 'HelpTooltip'
    if HelpTooltipResource.tooltips[key] then return HelpTooltipResource.tooltips[key]
    try
      HelpTooltipResource.tooltips[key] = yield HelpTooltipModel.findOne(key: key)
    catch e
      HelpTooltipResource.tooltips[key] = new HelpTooltipModel(key: key)
    return HelpTooltipResource.tooltips[key]


  saveTooltips: ()-> async ->
    promises = []
    for key, tooltip of HelpTooltipResource.tooltips
      promises.push tooltip.save()
    yield all promises
    app.notifier.notify 'Настройки успешно сохранены'

enableChangeTriggerForContenteditables = ->
  $('body').on('focus', '[contenteditable]', ->
    $this = $(this)
    $this.data 'before', $this.html()
    return $this
  ).on 'blur keyup paste input', '[contenteditable]', ->
    $this = $(this)
    if $this.data('before') isnt $this.html()
      $this.data 'before', $this.html()
      $this.trigger('change')

    return $this


clearPopovers = utils.prop false
$('body').on 'click', (e)->
  view.compute ->
    clearPopovers true

module.exports =
  controller: (attrs)->
    vm = @
    vm.title = utils.prop undefined
    vm.content = utils.prop undefined
    vm.show = utils.prop(false)
    vm.offset = utils.prop undefined
    vm.startShowTimer = (tooltipButton)->
      log 'start show timer'
      vm.stopShowTimer()
      vm.timeout = setTimeout vm.showPopup.bind(@, tooltipButton), 500

    vm.stopShowTimer = ()->
      log 'stop show timer'
      if vm.timeout
        clearTimeout(vm.timeout)
        vm.timeout = undefined


    vm.showPopup = (tooltipButton)-> async ->
      clearPopovers(false)
      $(tooltipButton).parent().addClass 'is-fetching'
      vm.tooltip = yield HelpTooltipResource.getOrCreateTooltip(attrs.key)
      $('.help-tooltip-group').removeClass 'is-fetching'
      vm.offset $(tooltipButton).parent().position()
      vm.show true

    return @


  view: (vm, attrs, children) ->
    do ->
      if clearPopovers()
        vm.show false
      span
        span
          if not window.__HACKhideTooltip?
            span.help-tooltip
              onmouseover: (e) -> vm.startShowTimer(e.target)
              onmouseout: (e) -> vm.stopShowTimer()

              span.help-tooltip-group
                span.help-tooltip-button.badge.badge-info | ?
                span.loader

        if vm.show()
            if app.admin.enableTooltipsChange()
              .popover.fade.right.in(role='tooltip')
                style:
                  top: (vm.offset().top - 35) + 'px'
                  left: vm.offset().left + 25 + 'px'
                  display: 'block'
                onclick: (e)-> e.stopPropagation()
                .arrow(style='top: 40%;')
                h3.popover-title(contenteditable='')
                  oninput: (e)->
                    vm.tooltip().title(e.target.innerText)
                  vm.tooltip().title() or 'Не задан'
                .popover-content(contenteditable='')
                  oninput: (e)->
                    vm.tooltip().content(e.target.innerText)
                  vm.tooltip().content() or 'Не задан'
            else
              if vm.tooltip().title()?
                .popover.fade.right.in(role='tooltip')
                  style:
                    top: (vm.offset().top - 35) + 'px'
                    left: vm.offset().left + 25 + 'px'
                    display: 'block'
                  onclick: (e)-> e.stopPropagation()
                  .arrow(style='top: 40%;')
                  h3.popover-title
                    vm.tooltip().title() or 'Не задан'
                  .popover-content
                    vm.tooltip().content() or 'Не задан'









