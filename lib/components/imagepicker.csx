utils = require '../utils'
view = require 'apphire-view'

uploadCrop = null
Croppie = require './croppie'
require('./croppie.css')
module.exports =
  controller: (attrs)->
    log 'PICKER CTRL INIT'
    vm = @
    vm.label = attrs.label or attrs.model.schema.caption or ''
    vm.helpText = attrs.helpText or attrs.model.schema.description or ''
    vm.showEditor = utils.prop false

    return @

  view: (vm, attrs, children) ->
    div
      label.control-label
        vm.label
      uma-help-tooltip
        key: attrs.helpTooltipKey
      div
        if attrs.model()
          .image-thumbnail
            onmousedown: ->
              vm.imageToCrop = attrs.model()
              vm.showEditor true
            img
              src: attrs.model()
        else
          button.btn.btn-primary
            type: "button"
            onmousedown: ->
              vm.imageToCrop = attrs.model()
              vm.showEditor true
            | Загрузить изображение
        div
          if vm.showEditor()
            uma-modal-box
              show: vm.showEditor
              .modal-header
                button.close(type='button')
                  onclick: -> vm.showEditor false
                  span | ×
                  span.sr-only | Закрыть
                h4.modal-title | Выберите изображение

              .modal-body
                .row
                  form(name='imageEditorForm')
                    'style':
                      'max-height': '300px'
                      overflow: 'auto'
                    .form-group
                      if not vm.imageToCrop
                        h4.text-center
                          span
                            'Перетащите файл в это окно или нажмите кнопку ниже'
                      else
                        h4
                          a.text-danger
                            onclick: ->
                              vm.imageToCrop = null
                              attrs.model null
                              vm.showEditor false
                              app.notifier.notify 'Изображение удалено'
                            | Удалить
                      input.text-center#image-upload(type='file')
                        'style': margin: '0 auto'
                        onchange: ->
                          vm.imageToCrop = null
                          if @files and @files[0]
                            reader = new FileReader

                            reader.onload = (e) ->
                              view.compute ->
                                vm.imageToCrop = e.target.result

                            reader.readAsDataURL @files[0]
                          else
                            alert 'Sorry - you\'re browser doesn\'t support the FileReader API'

                    if vm.imageToCrop?
                      .image-crop-area
                        key: 'loaded' #Это нужно, чтобы мифрил не считал элементы одним и тем же
                        config: (el, inited, context)->
                          if not inited
                            uploadCrop = new Croppie el,
                              viewport: width: 200, height: 200, type: 'circle'
                              boundary: width: 300,  height: 300

                            uploadCrop.bind url: vm.imageToCrop
                            context.onunload = ->
                              uploadCrop.destroy()

                    else
                      .image-crop-area
                        key: 'placeholder' #Это нужно, чтобы мифрил не считал элементы одним и тем же
                        config: (el2, inited2, context2)->
                          if not inited2
                            dragdrop el2,
                              onchange: (f) ->
                                reader2 = new FileReader
                                reader2.onload = (event) ->
                                  view.compute ->
                                    vm.imageToCrop = event.target.result
                                reader2.readAsDataURL f[0]
                        .image-crop-placeholder
                          i.fa.fa-upload.no-margin.big-icon
              .modal-footer
                button.btn.btn-primary(type='button', title='OK')
                  onclick: ->
                    view.compute()
                    uploadCrop.result({type: 'canvas',  size: 'viewport'}).then (resp) ->
                      attrs.model resp
                      vm.showEditor false
                      view.compute()
                  | OK
                button.btn.btn-white(type='button', title='Отмена')
                  onclick: -> vm.showEditor false
                  | Отмена



dragdrop = (element, options) ->

  activate = (e) ->
    e.preventDefault()
    return

  deactivate = ->

  update = (e) ->
    e.preventDefault()
    if typeof options.onchange == 'function'
      options.onchange (e.dataTransfer or e.target).files

  options = options or {}
  element.addEventListener 'dragover', activate
  element.addEventListener 'dragleave', deactivate
  element.addEventListener 'dragend', deactivate
  element.addEventListener 'drop', deactivate
  element.addEventListener 'drop', update
