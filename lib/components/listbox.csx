module.exports =
  controller: (attrs)->
    vm = @
    vm.options = attrs.options or attrs.model.schema.options[0]
    vm.label = attrs.label or attrs.model.schema.caption
    return vm

  view: (vm, attrs)->
    div
      label.control-label
        vm.label
      span
        uma-help-tooltip
          key: attrs.helpTooltipKey
      ~select.form-control
        multiple: attrs.multiple or 'multiple'
        size: attrs.size or 10
        onchange: (e)->
          selected = (opt.value for opt in e.target.options when opt.selected)
          attrs.model(selected)
          if attrs.onchange? then attrs.onchange(e)
        for k, v of vm.options()
          ~option
            'style':
              'background-color': 'red'
            title: v
            value: k
            selected: (attrs.model().indexOf(k) > -1)
            v


      span.help-block
        vm.helpText