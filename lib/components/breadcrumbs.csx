leafs = []

module.exports =
  reset: ()->
    leafs = []
  add: (leaf)->
    leafs.push(leaf)
  controller: (attrs)->
    vm = @
    return vm
  view: (vm, attrs, children)->
    .row.border-bottom.page-heading.wrapper.white-bg
      .col-lg-12
        h2.title
          leafs[leafs.length-1].caption
        ol.breadcrumb
          route = ''
          leafs.map (leaf, index)->
            if leaf.route?
              route = route + '/' + leaf.route
            closuredRoute = route
            if leaf.caption?
              li
                className: 'active' if index >= leafs.length - 1
                if index >= leafs.length - 1
                  strong
                    leaf.caption
                else
                  a
                    href: closuredRoute
                    leaf.caption
