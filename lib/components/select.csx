mithtrilBootstrap = require './mithril-bootstrap'
module.exports =
  controller: (attrs)->
    vm = @
    vm.label = attrs.label or attrs.model.schema?.caption or ''
    vm.helpText = attrs.helpText or attrs.model.schema?.description or ''
    vm.options = attrs.options or attrs.model.schema.options[0]
    return @


  view: (vm, attrs, children) ->
    .form-group.label-floating
      label.control-label
        vm.label

      span
        uma-help-tooltip
          key: attrs.helpTooltipKey
      .dropdown
        config: mithtrilBootstrap.ui.configDropdown()
        .form-control.dropdown-toggle
          'style':
            'cursor': 'pointer'
          span
            vm.options[attrs.model()]
          span.caret
        ul.dropdown-menu.select-dropdown
          style: #FIXME - перенести в стили
            'max-height': '230px'
            'overflow-y': 'auto'
          for k, v of vm.options
            li
              a
                onclick: do ->
                  key = k
                  return ->
                    attrs.model(key)
                v

      span.help-block
        vm.helpText

