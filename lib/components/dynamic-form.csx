#if attrs.type is 'COLLAPSIBLE'
#  uma-collapsible
#    expand: true
#    do ->
#      caption:
#        p
#          sectionSchema.__meta.caption
#    div
#      uma-dynamic-section
#        model: attrs.model()[sectionName]
#      .hr-line-dashed
#else

module.exports =
  controller: ->
    vm = @
    vm.i = 0
    return @

  view: (vm, attrs, children) ->
    ~fieldset
      for sectionName, sectionSchema of attrs.model.schema
        if sectionName isnt '__meta'
          div
            div
              h3
                sectionSchema.__meta.caption
              uma-dynamic-section
                model: attrs.model()[sectionName]
              .hr-line-dashed




