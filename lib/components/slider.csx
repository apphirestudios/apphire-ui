noUiSlider = require 'nouislider'
require './wNumb'
view = require 'apphire-view'

module.exports =
  controller: (attrs)->
    vm = @
    vm.label = attrs.label or attrs.model.schema?.caption
    vm.helpText = attrs.helpText or attrs.model.schema?.description or ''
    return @

  view: (vm, attrs, children) ->
    div
      if vm.label?
        label.control-label
          vm.label
      uma-help-tooltip
        key: attrs.helpTooltipKey
      div
        config: (el, inited)->

          if not inited
            sliderOpts =
              start: attrs.model()
              tooltips: true
              connect: 'lower'
              range:
                'min': [attrs.model.schema?.min or attrs.controlOptions?.min or 0]
                'max': [attrs.model.schema?.max or attrs.controlOptions?.max or 100]
              step: attrs.controlOptions?.step or 1
              format: wNumb
                decimals: 0
                postfix: if attrs.postfix then attrs.postfix

            if attrs.format then sliderOpts.format = attrs.format
            noUiSlider.create el, sliderOpts

            el.noUiSlider.on 'end', (value)->
              view.redraw()
            el.noUiSlider.on 'slide', (value)->
              attrs.model el.noUiSlider.get()
              if attrs.onSlide then attrs.onSlide(value[0])


          el.noUiSlider.set attrs.model()
      #.help-block
      #  vm.helpText

