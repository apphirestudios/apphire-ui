utils = require '../utils'

module.exports =
  controller: ->
    vm = @
    vm.key = Math.random()
    vm.key2 = Math.random()
    vm.focused = utils.prop false
    vm.constructClassName = (attrs)->
      classes = []
      if not attrs.model.valid?() and (attrs.model.touched?() or attrs.showValidationErrors?()) then classes.push 'has-error'
      if attrs.model.valid?() and attrs.model.touched?() then classes.push 'has-success'
      if vm.focused() then classes.push 'is-focused'
      if not attrs.model() then classes.push 'is-empty'
      return classes.join(' ')

    vm.constructAttributes = (attrs)->
      inputAttrs = {}
      inputAttrs.key = vm.key2
      if attrs.focusOn then inputAttrs.focusOn = true
      if attrs.inputClassName then inputAttrs.className = attrs.inputClassName
      if attrs.type then inputAttrs.type = attrs.type
      inputAttrs.value = attrs.model() or ''
      inputAttrs.onfocus = -> vm.focused true
      inputAttrs.onblur = -> vm.focused false
      inputAttrs.oninput = (e)->
        attrs.model(e.target.value)
        attrs.showValidationErrors?(false)
        attrs.oninput?(e)
      if attrs.inputConfig then inputAttrs.config = attrs.inputConfig
      return inputAttrs

    return @

  view: (vm, attrs, children) ->
    .form-group.label-floating
      className: vm.constructClassName(attrs)
      label.control-label
        attrs.label or attrs.model.schema.caption or ''
      span
        uma-help-tooltip
          key: attrs.helpTooltipKey
      input.form-control
        vm.constructAttributes(attrs)
      if (attrs.helpText or attrs.model.schema.description) and not attrs.showValidationErrors?()
        span.help-block
          attrs.helpText or attrs.model.schema.description or ''
      if attrs.showValidationErrors?() and not attrs.model.valid?()
        if attrs.model.valid?.error()
          span.error-block
            attrs.model.valid?.error()

