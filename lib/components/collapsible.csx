utils = require '../utils'

module.exports =
  controller: (attrs)->
    vm = @
    vm.expanded = utils.prop attrs.expand
    return @

  view: (vm, attrs, children) ->

    .panel.panel-default
      .panel-heading
        onmousedown: attrs.onclick or ->
          if attrs.expanded
            attrs.expanded not attrs.expanded()
          else
            vm.expanded not vm.expanded()
        h5.panel-title
          a.collapsed
            attrs?.caption or 'Группа'
      .panel-collapse.collapse
        className: 'in' if attrs.expanded?() or vm.expanded()
        .panel-body
          children
