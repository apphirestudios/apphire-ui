constants = require '../common/constants'
view = require 'apphire-view'

require 'spectrum-colorpicker'
require('./spectrum.css')
$ = require 'jquery'

module.exports =
  controller: (attrs)->
    vm = @
    vm.label = attrs.label or attrs.model.schema.caption or ''
    vm.helpText = attrs.helpText or attrs.model.schema.description or ''
    return @

  view: (vm, attrs, children) ->
     div
      label.control-label
        vm.label
      uma-help-tooltip
        key: attrs.helpTooltipKey
      div
        input
          type: 'text'
          config: (el, inited, context)->
            if not inited
              $(el).spectrum
                color: attrs.model()
                showButtons: false
                #allowEmpty:true
                showAlpha: true
                showPalette: true
                palette: attrs.palette or constants.colorPickerPalette
                move: (color)->
                  if attrs.autoredraw is true or attrs.autoredraw? is false
                    view.compute ->
                      if color?
                        attrs.model color.toHexString()
                      else
                        attrs.model undefined




