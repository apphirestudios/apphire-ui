module.exports =
  view: (vm, attrs, children) ->
    div
      config: (el, inited, ctx)->
        if not inited
          document.body.appendChild(el)
          ctx.onunload = ->
            try
              document.body.removeChild(el)
            catch e


      if children?
        .modal.inmodal.modal-open
          tabindex:'-1'
          .in.modal-backdrop
            onclick: -> attrs?.onClose?()
          .modal-dialog
            if attrs.dialogClass then className: attrs.dialogClass
            .modal-content.animated.slideInUp
              style:
                "animation-duration": "0.25s"
              children






