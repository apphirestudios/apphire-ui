utils = require '../utils'

module.exports =
  controller: ->
    vm = @
    vm.key = utils.guid()
    return vm
  view: (vm, attrs, children) ->
    div
      key: vm.key
      .switch
        .onoffswitch
          input.onoffswitch-checkbox(type='checkbox')
            id: vm.key
            checked: attrs.model()
            onchange: (e)->
              attrs.model e.target.checked
              if attrs.onchange then attrs.onchange(e.target.checked)
          label.onoffswitch-label
            'for': vm.key
            span.onoffswitch-inner
            span.onoffswitch-switch
