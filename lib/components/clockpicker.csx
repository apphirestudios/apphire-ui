require('./bootstrap-clockpicker.css')
require('./bootstrap-clockpicker')
view = require 'apphire-view'

module.exports =
  controller: ->
    vm = @
    vm.picker = undefined
    return vm
  view: (vm, attrs, children) ->
    .input-group.clockpicker
      config: (el, inited, ctx)->
        if not vm.picker
          if attrs.enabled?()
            vm.picker = $(el).clockpicker
              donetext: 'ОК'
              autoclose: true
              afterDone: (e)->
                view.compute ->
                  attrs.model $(el).find('input').val()
                  if attrs.onchange then attrs.onchange $(el).find('input').val()

        else
          if not attrs.enabled?()
            vm.picker.clockpicker('remove')
            vm.picker = null
      input.form-control(type='text')
        value: attrs.model()
        oninput: (e)->
          vm.picker.clockpicker('hide')
          attrs.model(e.target.value)
          if attrs.onchange then attrs.onchange e.target.value

        if not attrs.enabled?() then disabled: true
      style
        '.clockpicker-popover {z-index: 5000}'
      a.btn.input-group-addon
        if not attrs.enabled?() then disabled: true, className: 'btn-default'
        span.glyphicon.glyphicon-time


