view = require 'apphire-view'

module.exports =
  controller: ->
    vm = @
    vm.i = 0
    return @

  view: (vm, attrs, children) ->
    div
      for attrName, attrSchema of attrs.model.schema
          if attrName isnt '__meta'
            if (attrSchema instanceof Array)
              div
                h4
                  attrSchema[0].__meta.caption
                x = for subModel, index in attrs.model()[attrName]()
                  .row
                    .col-sm-11
                      for subAttrName, subAttrSchema of attrSchema[0]
                        if subAttrName isnt '__meta'
                          z =
                            type: 'jade'
                            tag: 'uma-' + subAttrSchema.controlType
                            sub: [
                              type: 'func'
                              func: ->
                                model:subModel()[subAttrName]
                                controlOptions: subAttrSchema
                                helpTooltipKey: attrs.helpTooltipPrefix + '.' + attrName + '[].' + subAttrName
                            ]
                          view.construct(z, z)

                    do -> #Кложура нужна, чтобы прикопать путь к модели
                      deleteButtonModel = attrs.model()[attrName]()
                      deleteButtonIndex = index
                      .col-sm-1
                        label.col-sm-1.control-label
                        a.input-group-link.text-danger
                          onclick: ->
                            deleteButtonModel.splice deleteButtonIndex, 1
                          i.fa.fa-minus-circle
                div
                do -> #Кложура нужна, чтобы прикопать путь к модели
                  addButtonAttrName = attrName
                  h5
                    a.text-primary
                      onclick: (e) ->
                        attrs.model()[addButtonAttrName]().push({})
                      i.fa.fa-plus-circle
                      span |  Добавить
            else
              z =
                type: 'jade'
                tag: 'uma-' + attrSchema.controlType
                sub: [
                  type: 'func'
                  func: ->
                    model: attrs.model()[attrName]
                    controlOptions: attrSchema
                    helpTooltipKey: attrs.helpTooltipPrefix + '.' + attrName

                ]
              view.construct(z, z)