require('./bootstrap-datepicker.css')
require('bootstrap-datepicker')
view = require 'apphire-view'

$ = require 'jquery'
module.exports =
  controller: ->
    vm = @
    vm.picker = undefined
    return vm
  view: (vm, attrs, children) ->
    .form-group.label-floating
      label.control-label
          attrs.label or attrs.model.schema.caption or ''
        span
          uma-help-tooltip
            key: attrs.helpTooltipKey
      input.datepicker.form-control
        config: (el, inited, ctx)->
          if not inited
            vm.picker = $(el).datepicker
              format: 'dd/mm/yyyy'
            vm.picker.on 'changeDate', (e)->
              attrs.model $(el).datepicker('getDate')
              view.redraw()
            $(el).datepicker('setDate', new Date(attrs.model()))
          if attrs.model()?
            if attrs.model().getTime() isnt $(el).datepicker('getDate')?.getTime()
              log 'DT SET'
              $(el).datepicker('setDate', new Date(attrs.model()))
          else
            if $(el).datepicker('getDate')?
              $(el).datepicker('clearDates')

