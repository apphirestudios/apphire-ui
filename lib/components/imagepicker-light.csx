utils = require '../utils'
view = require 'apphire-view'

uploadCrop = null
Croppie = require './croppie'
require('./croppie.css')
module.exports =
  controller: (attrs)->
    vm = @
    vm.label = attrs.label or attrs.model.schema.caption or ''
    vm.helpText = attrs.helpText or attrs.model.schema.description or ''
    return @

  view: (vm, attrs, children) ->
    div
      if attrs.showEditor()
        uma-modal-box
          .modal-header
            config: (el, inited)->
              if not inited
                vm.imageToCrop = attrs.model()
            button.close(type='button')
              onclick: -> attrs.showEditor false
              span | ×
              span.sr-only | Закрыть
            h4.modal-title | Выберите изображение

          .modal-body
            .row
              form(name='imageEditorForm')
                .form-group
                  if not vm.imageToCrop
                    h4.text-center
                      span
                        'Перетащите файл в это окно или нажмите кнопку ниже'
                  else
                    h4
                      a.text-danger
                        onclick: ->
                          vm.imageToCrop = null
                          attrs.model null
                          attrs.showEditor false
                          app.notifier.notify 'Изображение удалено'
                          return
                        | Удалить
                  input.text-center#image-upload(type='file')
                    'style': margin: '0 auto'
                    onchange: ->
                      vm.imageToCrop = null
                      if @files and @files[0]
                        reader = new FileReader

                        reader.onload = (e) ->
                          view.compute ->
                            vm.imageToCrop = e.target.result

                        reader.readAsDataURL @files[0]
                      else
                        alert 'Sorry - your browser doesn\'t support the FileReader API'

                if vm.imageToCrop?
                  .image-crop-area
                    key: 'loaded' #Это нужно, чтобы мифрил не считал элементы одним и тем же
                    config: (el, inited, context)->
                      if not inited
                        uploadCrop = new Croppie el,
                          viewport: width: 200, height: 200, type: 'circle'
                          boundary: width: 300,  height: 300

                        uploadCrop.bind url: vm.imageToCrop
                        context.onunload = ->
                          uploadCrop.destroy()

                else
                  .image-crop-area
                    key: 'placeholder' #Это нужно, чтобы мифрил не считал элементы одним и тем же
                    config: (el2, inited2, context2)->
                      if not inited2
                        dragdrop el2,
                          onchange: (f) ->
                            reader2 = new FileReader
                            reader2.onload = (event) ->
                              view.compute ->
                                vm.imageToCrop = event.target.result
                            reader2.readAsDataURL f[0]
                    .image-crop-placeholder
                      i.fa.fa-upload.no-margin.big-icon
          .modal-footer
            button.btn.btn-primary(type='button', title='OK')
              onclick: ->
                resp = yield uploadCrop.result({type: 'canvas',  size: 'viewport'})
                attrs.model resp
                attrs.showEditor false

              | OK
            button.btn.btn-white(type='button', title='Отмена')
              onclick: -> attrs.showEditor false
              | Отмена



dragdrop = (element, options) ->

  activate = (e) ->
    e.preventDefault()
    return

  deactivate = ->

  update = (e) ->
    e.preventDefault()
    if typeof options.onchange == 'function'
      options.onchange (e.dataTransfer or e.target).files

  options = options or {}
  element.addEventListener 'dragover', activate
  element.addEventListener 'dragleave', deactivate
  element.addEventListener 'dragend', deactivate
  element.addEventListener 'drop', deactivate
  element.addEventListener 'drop', update
