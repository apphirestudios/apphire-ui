module.exports =
  view: (vm, attrs, children) ->
    div
      if app.auth.isAdmin()
        .theme-config
          .theme-config-box
            className: 'show' if vm.show
            .spin-icon
              onclick: -> vm.show = not vm.show
              i.fa.fa-cogs.fa-spin
            .skin-setttings
              .title | Настройка
              .setings-item
                span
                  | Переводчик
                .switch
                  .onoffswitch
                    input#collapsemenu.onoffswitch-checkbox(type='checkbox', name='collapsemenu')
                      checked: app.admin.enableTranslateBox()
                      onchange: (e)->  app.admin.enableTranslateBox e.currentTarget.checked
                    label.onoffswitch-label(for='collapsemenu')
                      span.onoffswitch-inner
                      span.onoffswitch-switch
                p
                  a | Сохранить
                    disableTranslateBox: true
                    onclick: -> app.translator.saveTranslations()
              .setings-item
                span
                  | Подсказки
                .switch
                  .onoffswitch
                    input#tooltips.onoffswitch-checkbox(type='checkbox', name='tooltips')
                      checked: app.admin.enableTooltipsChange()
                      onchange: (e)-> app.admin.enableTooltipsChange
                    label.onoffswitch-label(for='tooltips')
                      span.onoffswitch-inner
                      span.onoffswitch-switch
                p
                  a | Сохранить
                    onclick: -> attrs.saveTooltipsFn()
              .setings-item
                span
                  | Перетаскивание
                .switch
                  .onoffswitch
                    input#fixedsidebar.onoffswitch-checkbox(type='checkbox', name='fixedsidebar')
                      checked: app.admin.enableMove()
                      onchange: (e)-> app.admin.enableMove e.currentTarget.checked
                    label.onoffswitch-label(for='fixedsidebar')
                      span.onoffswitch-inner
                      span.onoffswitch-switch
              .setings-item
                span
                  | Растягивание
                .switch
                  .onoffswitch
                    input#fixednavbar.onoffswitch-checkbox(type='checkbox', name='fixednavbar')
                      checked: app.admin.enableResize()
                      onchange: (e)->  app.admin.enableResize e.currentTarget.checked
                    label.onoffswitch-label(for='fixednavbar')
                      span.onoffswitch-inner
                      span.onoffswitch-switch




