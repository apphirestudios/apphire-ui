toastr = require 'toastr'

require 'toastr/build/toastr.css'
toastr.options =
  "closeButton": true
  "debug": false
  "progressBar": true
  "preventDuplicates": false
  "positionClass": "toast-top-right"
  "onclick": null
  "showDuration": 500
  "hideDuration": 500
  "extendedTimeOut": 30000
  "showEasing": "swing"
  "hideEasing": "linear"
  "showMethod": "fadeIn"
  "hideMethod": "fadeOut"

module.exports =
  notify: (msg, type, timeOut) ->
    toastr.options.timeOut = timeOut or 3000
    toastr[type or 'success'](msg)
  error: (msg, timeOut) ->
    toastr.options.timeOut = timeOut or 3000
    toastr['error'](msg)
    return