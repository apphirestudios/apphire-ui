utils = require '../utils'

module.exports = translator =
  language: utils.prop 'ru'
  init: -> async ->
    translator.TranslationModel = yield app.Model 'Translation'
    translator.dictionary = yield translator.TranslationModel.find()

  translate: (text)->
    if translator.language() isnt 'ru'
      translation = translator.getTranslation text
      if translation
        translated = translation()[translator.language()]()
        if translated then return translated
    return text


  addTranslation: (base)->
    newTranslation = new translator.TranslationModel()
    newTranslation().base(base)
    translator.dictionary.push newTranslation
    return newTranslation
  getTranslation: (base)->
    res = undefined
    translator.dictionary.map (translation)->
      if translation().base() == base then res = translation
    return res

  saveTranslations: ()->
    promises = []
    for translation in translator.dictionary
      yield translation.save()
    app.notifier.notify 'Переводы успешно сохранены'
