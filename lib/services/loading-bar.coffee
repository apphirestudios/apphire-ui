
showingInterval = null
quenueClearing = false
percentage  = 0
maxTimeout = 10000 #Через это время бар сбросится даже если его никто не сбросит
timeoutToShowFullscreen = 3000 #Через это время бар превратится в тыкву, большую полноэкранную тыкву
timeoutToDebounce = 1000

#Если бар уже крутится и прилетает еще одна команда на его запуск до истечения этого таймаута,
# то мы не будем сбрасывать состояние бара на начальное
shownTime = 0

barHtml = $ '<div class="loading-bar" style="z-index:8888;
        position:fixed;
        left:0px;
        top:0px;
        height:4px;
        width:100%">
      <span style="background-color:#16987e;
      display:block;
      height:100%;
      position:relative;
      overflow:hidden">
      </span>
   </div>'


barFullScreen = $ '<div class="loader-fullscreen in modal-backdrop" style="z-index:9999!important;">
<svg version="1.1" id="L7" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
         <path d="M31.6,3.5C5.9,13.6-6.6,42.7,3.5,68.4c10.1,25.7,39.2,38.3,64.9,28.1l-3.1-7.9c-21.3,8.4-45.4-2-53.8-23.3
          c-8.4-21.3,2-45.4,23.3-53.8L31.6,3.5z">
              <animateTransform
                 attributeName="transform"
                 attributeType="XML"
                 type="rotate"
                 dur="2s"
                 from="0 50 50"
                 to="360 50 50"
                 repeatCount="indefinite" />
          </path>
         <path d="M42.3,39.6c5.7-4.3,13.9-3.1,18.1,2.7c4.3,5.7,3.1,13.9-2.7,18.1l4.1,5.5c8.8-6.5,10.6-19,4.1-27.7
          c-6.5-8.8-19-10.6-27.7-4.1L42.3,39.6z">
              <animateTransform
                 attributeName="transform"
                 attributeType="XML"
                 type="rotate"
                 dur="1s"
                 from="0 50 50"
                 to="-360 50 50"
                 repeatCount="indefinite" />
          </path>
         <path d="M82,35.7C74.1,18,53.4,10.1,35.7,18S10.1,46.6,18,64.3l7.6-3.4c-6-13.5,0-29.3,13.5-35.3s29.3,0,35.3,13.5
          L82,35.7z">
              <animateTransform
                 attributeName="transform"
                 attributeType="XML"
                 type="rotate"
                 dur="2s"
                 from="0 50 50"
                 to="360 50 50"
                 repeatCount="indefinite" />
          </path>
        </svg></div>'

barFullScreenForced = $ '<div class="loader-fullscreen-forced in modal-backdrop" style="z-index:9999!important;">
<svg version="1.1" id="L7" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
         <path d="M31.6,3.5C5.9,13.6-6.6,42.7,3.5,68.4c10.1,25.7,39.2,38.3,64.9,28.1l-3.1-7.9c-21.3,8.4-45.4-2-53.8-23.3
          c-8.4-21.3,2-45.4,23.3-53.8L31.6,3.5z">
              <animateTransform
                 attributeName="transform"
                 attributeType="XML"
                 type="rotate"
                 dur="2s"
                 from="0 50 50"
                 to="360 50 50"
                 repeatCount="indefinite" />
          </path>
         <path d="M42.3,39.6c5.7-4.3,13.9-3.1,18.1,2.7c4.3,5.7,3.1,13.9-2.7,18.1l4.1,5.5c8.8-6.5,10.6-19,4.1-27.7
          c-6.5-8.8-19-10.6-27.7-4.1L42.3,39.6z">
              <animateTransform
                 attributeName="transform"
                 attributeType="XML"
                 type="rotate"
                 dur="1s"
                 from="0 50 50"
                 to="-360 50 50"
                 repeatCount="indefinite" />
          </path>
         <path d="M82,35.7C74.1,18,53.4,10.1,35.7,18S10.1,46.6,18,64.3l7.6-3.4c-6-13.5,0-29.3,13.5-35.3s29.3,0,35.3,13.5
          L82,35.7z">
              <animateTransform
                 attributeName="transform"
                 attributeType="XML"
                 type="rotate"
                 dur="2s"
                 from="0 50 50"
                 to="360 50 50"
                 repeatCount="indefinite" />
          </path>
        </svg></div>'


update = ->
  percentage += (100-percentage)*Math.random()/100
  shownTime += 10
  barHtml.width percentage + '%'
  if shownTime > timeoutToShowFullscreen then showFullscreen()
  if shownTime > maxTimeout or quenueClearing
    if shownTime > timeoutToDebounce
      percentage = 100
      finalize =->
        percentage = 0
        shownTime = 0
        if showingInterval isnt null
          clearInterval showingInterval
          showingInterval = null
          $('body .loading-bar').remove()
          $('body .loader-fullscreen').remove()
          quenueClearing = false

      setTimeout finalize, 500

showFullscreen = ->
  $('body').append barFullScreen


show = (options)->
  if options?.timeoutToShowFullscreen
    timeoutToShowFullscreen = options.timeoutToShowFullscreen
  if showingInterval isnt null
    if shownTime > timeoutToDebounce
      percentage = 0
      shownTime = 0
  else
    showingInterval = setInterval update, 10
    update()
    $('body').append barHtml

clear = ->
  quenueClearing = true

showImmediate = ->
  $('body').append barFullScreenForced


clearImmediate = ->
  $('body .loader-fullscreen-forced').remove()

module.exports =
  show: show
  clear: clear
  showImmediate: showImmediate
  clearImmediate: clearImmediate
