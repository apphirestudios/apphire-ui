module.exports = (args)->
  if typeof args.children[0] is 'string' and not args.attrs?.notranslate
    #Отбросим пробелы в начале и конце строки
    textToTranslate = args.children[0].replace(/^\s\s*/, '').replace(/\s\s*$/, '')
    if app.translator
      args.children[0] = app.translator.translate textToTranslate
  return args