mithtrilBootstrap = require '../components/mithril-bootstrap'
module.exports = (args)->
  if typeof args.children[0] is 'string' and app.admin.enableTranslateBox() and not args.attrs?.disableTranslateBox
    textToTranslate = args.children[0].replace(/^\s\s*/, '').replace(/\s\s*$/, '')
    if not args.attrs?.wrappedForTranslate
      args.children[0] =
        span.dropdown
          config: mithtrilBootstrap.ui.configDropdown()
          span.dropdown-toggle
            wrappedForTranslate : true
            textToTranslate
          .dropdown-menu.animated.fadeInRight.m-t-xs.scope
            onclick: (e)->
              e.preventDefault()
              e.stopPropagation()
            .ibox-content
              'style':
                width: '400px'
              form.form-horizontal
                .form-group
                  .col-sm-2
                    img(src='images/flags/32/ru.png')
                  .col-sm-10
                    input.form-control
                      disabled: true
                      value: textToTranslate
                .form-group
                  .col-sm-2
                    img(src='images/flags/32/en.png')
                  .col-sm-10
                    input.form-control
                      value: app.translator.getTranslation(textToTranslate)?().en() or ''
                      onkeyup: (e)->
                        translation = app.translator.getTranslation(textToTranslate)
                        if translation is undefined then translation = app.translator.addTranslation(textToTranslate)
                        translation().en e.target.value

  return args