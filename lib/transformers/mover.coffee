module.exports = (args)->
  draggableSelectors = ['div', 'span', 'a', 'button', 'p', 'h1', 'h2', 'h3', 'h4']
  resizableSelectors = ['div']
  if app.admin.enableMove() or app.admin.enableResize()
    if args.attrs
      originalConfig = args.attrs.config
      args.attrs.config = (el, inited, context)->
        if not inited
          if app.admin.enableMove()
            draggableSelectors.map (selector)->
              if $(el).is selector
                $(el).draggable({cancel:false})
          if app.admin.enableResize()
            resizableSelectors.map (selector)->
              if $(el).is selector
                $(el).resizable
                  handles: 'n, e, s, w, ne, se, sw, nw'
        if originalConfig
          originalConfig el, inited, context
  args