module.exports = (selector, attrs, children) ->
  configOrig = attrs?.config
  attrs.config = (e, inited, ctx)->
    if not inited
      #В этой же функции нельзя, поскольку элемент еще не инициализировался
      #(привет ангуляр :) )
      setTimeout (-> e.focus()), 10
    if configOrig then configOrig(arguments)