module.exports = (selector, attrs, children) ->
  attrs.value = attrs.bidi?() or ''
  attrs.oninput = (e)->
    attrs.bidi(e.target.value)