module.exports = (selector, attrs, children) ->
  onclickOrig = attrs?.onclick
  if not onclickOrig then throw new Error 'Loader attribute without onclick handler on ' + selector
  attrs.onclick = (e)->
    $(e.target).append('<span class="loader"></span>')
    try
      yield async onclickOrig(e)
      $(e.target).find('.loader').remove()
    catch error
      $(e.target).find('.loader').remove()
      throw error