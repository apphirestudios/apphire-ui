module.exports = (selector, attrs, children)->
  attrs.style = attrs.style or {}
  attrs.style.visibility = if attrs.showIf then 'visible' else 'hidden'