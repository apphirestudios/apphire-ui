EventEmitter2 = require('eventemitter2').EventEmitter2
queryStringParser = require('query-string')
chai = require 'chai'
expect = chai.expect #TODO switch to more lightweight assertion library

isPlainObject = require 'is-plain-object'
isFunction = (obj) ->
  return not not (obj and obj.constructor and obj.call and obj.apply)

class DefinitionError extends Error
  constructor: (@message)-> super() 

class RuntimeError extends Error
  constructor: (@message)-> super() 

checkRoutesDescription = (routeDescription, currentHash)->
  throwError = (msg)->
    throw new DefinitionError 'Error defining route "' + (currentHash or '/') + '": ' + msg
  itIsLastRoute = true #This will be set to false if we'll get any child route definitions during parsing
  try
    expect(routeDescription).to.be.an 'object'
  catch e
    throwError 'Route definition should be an object, instead got ' + routeDescription
  if not routeDescription.module? and not routeDescription.redirect?
    throwError 'Description of route must have either "module" or "redirect" attribute'  

  for k, v of routeDescription
    switch k
      when 'redirect'
        try
          expect(v).to.be.a 'string'
        catch e
          throwError '"Redirect" attribute should be a string, instead got:  "' + v + '"'
        try
          expect(routeDescription[v]).to.be.a 'object'
        catch e
          throwError 'You are trying to redirect to route ' + v + ' which does not exist'
        try
          expect(routeDescription.module).to.not.exist
        catch e
          throwError 'You cannot have both "redirect" and "module" defined in same route'
      when 'module'
        if isPlainObject(v) or isFunction(v)
          if isPlainObject(v)
            if not v.view?
              throwError 'Module definition should have "view" attribute'            
            else if not isFunction(v.view)
              throwError 'Module\'s "view" attribute should be a function'
            if v.controller? and not  isFunction(v.controller)
              throwError 'Module\'s "controller" attribute should be a function'
        else
          throwError 'Module definition should be either object or function'
    
        try
          expect(routeDescription.redirect).to.not.exist
        catch e
          throwError 'You cannot have both "redirect" and "module" defined in same route'          
      else        
        if k[0] is ':'
          for k1, v1 of routeDescription when k isnt k1 and k1[0] is ':'
            throwError 'You are trying to define two parameterized routes: ' + k1 + ' and ' + k
        checkRoutesDescription(v, (currentHash or '') + '/' + k)      

         
getParameterizedRouteName = (desc)->
  for k, v of desc
    if k[0] is ':' then return k
  return undefined  

class Router extends EventEmitter2
  DefinitionError: DefinitionError
  RuntimeError: RuntimeError
  autostart: true
  constructor: (@routes)->
    if Router.autostart? then @autostart = Router.autostart
    checkRoutesDescription(@routes)    
    super
      wildcard: true
      delimiter: ':'
 
  mount: (@routeRoot, forcedRoute)-> async =>
    resolved = yield @navigate(forcedRoute or window.location.hash)  
  navigate: (hash) -> async =>
    hash = hash.replace(/(\/*)$/, "") #stripping slashes
    if @autostart
      window.onhashchange = null
      window.location.hash = hash

    @params = {}
    @query = {}    
    #we get hash like #/first/second so we need to strip hashbang and first slash
    fragments = hash.substring(1).split('/')
    current = @routes
    for fragment, index in fragments   
      if current.before?
        for fn in current.before
          yield fn()   
      if index isnt fragments.length - 1         
        nextFragment = (fragments[index+1]).split('?')[0] #Stripping query string  
        if current[nextFragment]?
          current = current[nextFragment]
        else if (parameterizedRouteName = getParameterizedRouteName(current))?
          current = current[parameterizedRouteName]
          @params[parameterizedRouteName.substring(1)] = nextFragment
        else
          throw new RuntimeError 'Route name ' + hash + ' not found'          
      else
        if current.redirect?
          currentHash = '#' + (fragment for fragment, i in fragments when i <= index).join('/')
          return @navigate(currentHash + '/' + current.redirect)
        else
          queryString = fragment.split('?')[1]
          if queryString? then @query = queryStringParser.parse(queryString)
          if @autostart
            window.onhashchange = =>
              @navigate window.location.hash            
            yield apphire.mount @routeRoot, current.module  
          
          return result =
            module: current.module          
            hash: hash
module.exports = Router