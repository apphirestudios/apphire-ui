module.exports =
  prop: (store) ->
    p = ->
      if arguments.length
        store = arguments[0]
      return store

    p.toJSON = ->
      return store
    return p
  guid: ->
    s4 = ->
      Math.floor((1 + Math.random()) * 0x10000).toString(16).substring 1
    s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4()